import numpy as np
import pandas as pd
from numpy import linalg as LA

def likelihood(x, mu, sigma):
	global alpha
	sigmaprime = (1 - alpha) * sigma + (alpha * np.trace(sigma) * np.identity(len(sigma)))/featuresNum
	xprime = np.matrix(x - mu)
	exponent = -0.5*(xprime * (LA.inv(sigmaprime)) * xprime.T)
	den = np.sqrt(LA.det(sigmaprime)) * ((2 * np.pi) ** (featuresNum/2))
	return np.exp(exponent)/den

def result(x, mu, sigma, prior):
	ans = []
	for i in range(NUMCLASS):
		ans.append(prior[i] * likelihood(x, mu[i], sigma[i]))
	return np.argmax(np.array(ans))

def acc(x, mu, sigma, prior, label, confusion):
	correct = 0
	for i in range(testDataNum):
		prediction = result(x[i], mu, sigma, prior)
		if(prediction == label[i]):
			correct += 1
		confusion[int(prediction)][int(label[i])] += 1
	return correct/testDataNum

data_train = 0.01 * np.array(pd.read_csv('Fashion-MNIST/Train_Data.csv', header=None))
label_train = np.array(pd.read_csv('Fashion-MNIST/Train_Labels.csv', header=None))
data_test = 0.01 * np.array(pd.read_csv('Fashion-MNIST/Test_Data.csv', header=None))
label_test = np.array(pd.read_csv('Fashion-MNIST/Test_Labels.csv', header=None))

trainDataNum = data_train.shape[0]
testDataNum = data_test.shape[0]
featuresNum = data_train.shape[1]
NUMCLASS = 10
alpha = 0

train = [np.zeros((1,featuresNum))] * NUMCLASS
for i in range(trainDataNum):
	for l in range(NUMCLASS):
		if label_train[i] == l:
			train[l] = np.append(train[l],[data_train[i]],axis=0)
for l in range(NUMCLASS):
	train[l] = np.delete(train[l],0,axis=0)


mu = []
for i in range(NUMCLASS):
	a = [data_train[j] for j in range(trainDataNum) if i == label_train[j]]
	mu.append(np.mean(a, axis = 0))

sigma = [0] * NUMCLASS
for i in range(NUMCLASS):
	b = [data_train[j] for j in range(trainDataNum) if i == label_train[j]]
	sigma[i] = np.cov(np.array(b).T)

prior = [0.1]*10
confusion1 = np.zeros((NUMCLASS, NUMCLASS))
print("without prior:%",acc(data_test, mu, sigma, prior, label_test, confusion1))
print(confusion1)

prior = []
for i in range(NUMCLASS):
	temp = len([data_train[j] for j in range(trainDataNum) if i == label_train[j]])/trainDataNum
	prior.append(temp)
confusion2 = np.zeros((NUMCLASS, NUMCLASS))
print("with prior:%",acc(data_test, mu, sigma, prior, label_test, confusion2))
print(confusion2)

