import numpy as np
import pandas as pd
from numpy import linalg as LA

train = pd.read_csv("HW3_DataSets/Q5/Mushroom_Train.csv", sep=',')
test = pd.read_csv("HW3_DataSets/Q5/Mushroom_Test.csv", sep=',')

NUMCLASS = len(list(set(train['class'])))
NUMDATA = len(train['class'])
NUMFEATURES = len(train.columns) - 1

features = train.columns


def naive(test, possiblities, FEATS, i):
	ans = 1
	for j in range(len(FEATS)):
		if j == 0:
			continue
		# print(FEATS[j])
		# print((np.array(test[test.columns[j]])))
		x = FEATS[j].index(np.array(test[test.columns[j]]))
		ans = ans * possiblities[j][x][i]
	return ans

def result(test, possiblities, prior, FEATS):
	ans = []
	for i in range(NUMCLASS):
		ans.append(prior[i] * naive(test, possiblities, FEATS, i))
	return np.argmax(np.array(ans))

def acc(test, possiblities, FEATS, prior, confusion):
	correct = 0
	for i in range(len(test)):
		confusion[result(test[i:i+1], possiblities, FEATS, prior)][1*(np.array(test[i:i+1]['class']) == 'e')] += 1
		if(result(test[i:i+1], possiblities, FEATS, prior) == 0 and np.array(test[i:i+1]['class']) == 'p'):
			correct += 1
			continue
		if((result(test[i:i+1], possiblities, FEATS, prior) == 1) and np.array(test[i:i+1]['class'] == 'e')):
			correct += 1
			continue
	return correct/len(test)



prior = [0, 0]
prior[0] = (len(train[train['class'] == 'p']))/NUMDATA
prior[1] = (len(train[train['class'] == 'e']))/NUMDATA

possiblities = []
FEATS = []
for i in features:
	c = []
	feat = list(set(train[i]))
	FEATS.append(feat)
	for j in range(len(feat)):
		a = train[train['class'] == 'p']
		qp = (len(a[a[i] == feat[j]]) + 1)/(len(a) + len(feat))
		a = train[train['class'] == 'e']
		qe = (len(a[a[i] == feat[j]]) + 1)/(len(a) + len(feat))
		c.append([qp, qe])
	possiblities.append(c)

confusion = np.zeros((NUMCLASS, NUMCLASS))
print("accuracy is %", 100 * acc(test, possiblities, prior, FEATS, confusion))
print(confusion)

